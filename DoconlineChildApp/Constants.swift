//
//  Constants.swift
//  DoconlineChildApp
//
//  Created by Santosh Kumar on 06/07/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import Foundation
import Firebase


@IBDesignable class GradientView: UIView {
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpUI()
    }
    
    func setUpUI(){
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [UIColor.red,UIColor.red]
//        gradientLayer.colors = Theme.navigationGradientColor as! [UIColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0)
    }
    
    override func layoutSubviews() {
        
    }
    

    
}



struct BookingConsultation {
    ///Type internet call
    public static var INTERNET_CALL = 1
    ///Type regular call or knowlarity call
    public static var REGULAR_CALL = 2
    ///Type audio call
    public static var CALL_TYPE_AUDIO = 1;
    ///Type video call
    public static var CALL_TYPE_VIDEO = 2;
    ///Type consultation for self
    public static var CONSULTATION_SELF = 1;
    ///type of consultation for family
    public static var CONSULTATION_FAMILY = 2;
    ///type of booking WAIT
    public static var BOOKING_TYPE_WAIT = 1
    ///type of booking SCHEDULE with time and date
    public static var BOOKING_TYPE_SCHEDULE = 2
    ///Appointment status is active
    public static var STATUS_APPOINTMENT_ACTIVE = 1
    ///Appointment status CANCELLED
    public static var STATUS_APPOINTMENT_CANCELLED = 0
    ///Appointment status call not attended
    public static var STATUS_NOT_ATTENDED   =   9
    ///Appointment status call not placed by doctor
    public static var STATUS_CALL_NOT_PLACED =   4
    ///Appointment status call not placed by doctor
    public static var STATUS_CALL_FINISHED =   2
    ///Appointment status preferred languages yes
    public static var STATUS_PREFERRED_LANGUAGES_YES   =   1
    ///Appointment status preferred languages no
    public static var STATUS_PREFERRED_LANGUAGES_NO =   0
    ///Appointment status call Placed no prescription
    public static let STATUS_CALL_PLACED_NO_PRESCRIPTION =   10;
 
}

var BaseUrl = AppEnvironment.shared().baseURL  //RemoteConfig.remoteConfig().configValue(forKey: RemoteConfigKeys.BASE_URL).stringValue ?? "https://app.doconline.com/"
/**
   Remote config value keys
 */
struct RemoteConfigKeys {
    static let BASE_URL = "base_url"
    static let CLIENT_SECRET = "client_secret"
    static let RINGING_DURATION = "ringing_duration"
    static let CLIENT_ID = "client_id"
    static let FILE_ATTACHMENT_LIMIT = "file_attachment_limit"
    static let APPOINTMENT_CALL_BACK_LIMIT = "appointment_call_back_time_limit"
    static let OPENTOK_KEY = "opentok_api_key"
    static let FB_CLIENT_ID = "fb_client_id"
    static let FB_CLIENT_SECRET = "fb_client_secret"
    static let ENV_PREFIX = "env_prefix"
    static let RZPAY_KEY = "razorpay_api_key"
    static let RZPAY_SECRET = "razorpay_api_secret"
    static let CUSTOMER_CARE_NUMBER = "customer_care_number"
    static let MAX_FILE_SIZE = "max_file_size"
}


struct Keys {
    //Family count
    static let HOTLINE_NUMBER =  "hotline_number"
    static let FAMILY_MEMBERS_COUNT = "family_members_count"
    static let FAMILY_ALLOWED_COUNT = "family_members_allowed"
    
    //consent form check family
    static let KEY_BOOKING_CONSENT = "booking_consent"
    
    ///OTP discount Coupon code keys
    static let KEY_DISCOUNT_CODE = "discount_code"
    static let KEY_PLAN_AMOUNT = "plan_amount"
    static let KEY_DISCOUNT = "discount"
    static let KEY_TITLE = "title"
    static let FILE_URL = "file_url"
    
    ///Unregistered user Booking view keys
    static let KEY_EMAIL_VERIFIED = "email_verified"
    static let KEY_IS_VERIFIED = "is_verified"
    static let KEY_IS_ACTIVE = "is_active"
    static let KEY_VALIDATED = "validated"
    static let KEY_APPOINTMENT = "appointment"
    static let KEY_TIMEZONE = "timezone"
    static let KEY_RAZORPAY_DATA = "razorpay_data"
    static let KEY_PLAN = "plan"
    static let KEY_PLAN_ID = "plan_id"
    static let KEY_DOC_USER = "doc_user"
    static let KEY_REGISTERED = "registered"
    static let KEY_ACTIVATION_TYPE = "activation_type"
    static let KEY_PLATFORM = "platform"
    static let KEY_PREFILL = "prefill"
    static let KEY_IMAGE = "image"
    static let KEY_RAZORPAY_SIGNATURE = "razorpay_signature"
    static let KEY_RAZORPAY_PAYEMENT_ID = "razorpay_payment_id"
    static let KEY_RAZORPAY_SUBSCRIPTION_ID = "razorpay_subscription_id"
    static let KEY_PAYMENT_GATEWAY = "payment_gateway"
    static let KEY_EXCEPTION = "exception"
    
    //billing
    static let KEY_DISPLAY_COLOR = "display_color"
    static let KEY_EXTRA = "extra"
    static let KEY_DISPLAY_PRICE = "display_price"
    static let KEY_DISPLAY_PERIOD = "display_period"
    static let KEY_DISPLAY_NAME = "display_name"
    static let KEY_DOWNLOAD_URL = "download_url"
    static let KEY_BILLING_START = "billing_start"
    static let KEY_BILLING_END = "billing_end"
    static let TAX_INCLUSIVE = "tax_inclusive"
    static let KEY_NET_AMOUNT = "net_amount"
    static let KEY_ITEM_ID = "item_id"
    static let KEY_ENTITY = "entity"
    static let KEY_RECIEPT = "receipt"
    static let KEY_INVOICE_NUMBER = "invoice_number"
    static let KEY_CUSTOMER_ID = "customer_id"
    static let KEY_CUSTOMER_DETAILS = "customer_details"
    static let KEY_BILLING_ADDRESS = "billing_address"
    static let KEY_CONTACT = "contact"
    static let KEY_SUBSCRIPTION_ID = "subscription_id"
    static let KEY_LINE_ITEMS = "line_items"
    static let KEY_ITEMS = "items"
    static let KEY_PAYMENT_ID = "payment_id"
    static let KEY_EXPIRED_BY = "expire_by"
    static let KEY_ISSUED_AT = "issued_at"
    static let KEY_PAID_AT = "paid_at"
    static let KEY_CANCELLED_AT = "cancelled_at"
    static let KEY_EXPIRED_AT = "expired_at"
    static let KEY_SMS_STATUS = "sms_status"
    static let KEY_EMAIL_STATUS = "email_status"
    static let KEY_GROSS_AMOUNT = "gross_amount"
    static let KEY_TAX_AMOUNT = "tax_amount"
    static let KEY_AMOUNT_PAID = "amount_paid"
    static let KEY_AMOUNT_DUE = "amount_due"
    static let KEY_PATH = "path"
    
    static let KEY_PENDING_SUBSCRIPTION = "pending_subscription"
    
    static let KEY_RAZORPAY_ORDER_ID = "razorpay_order_id" //newly added for one time payment
    
    static let KEY_FEAUTURED = "featured"
    static let KEY_PLAN_NAME = "plan_name"
    static let KEY_PAID_COUNT = "paid_count"
    static let KEY_CHARGE_AT = "charge_at"
    static let KEY_TRIAL_ENDS_AT = "trial_ends_at"
    static let KEY_RAZORPAY_PLAN_ITEM = "razorpay_plan_item"
    static let KEY_ACTIVE = "active"
    static let KEY_QUANTITY = "quantity"
    static let KEY_RAZORPAY_PLAN = "razorpay_plan"
    static let KEY_RAZORPAY_ID = "razorpay_id"
    static let KEY_ITEM = "item"
    static let KEY_AMOUNT = "amount"
    static let KEY_UNIT_AMOUNT = "unit_amount"
    static let KEY_PERIOD = "period"
    static let KEY_DESCRIPTION = "description"
    static let KEY_EXTERNAL_WALLET = "external"
    static let KEY_CURRENCY = "currency"
    static let KEY_TOTAL_COUNT = "total_count"
    static let KEY_CROSS_PRICE = "cross_price"
    static let KEY_DISCOUNT_TEXT = "discount_text"
    static let KEY_PACKAGES = "packages"
    static let KEY_INTERVAL = "interval"
    static let KEY_INTERNAL_ORDER = "internal_order"
    static let KEY_CAN_UPGRADE = "can_upgrade"
    static let KEY_USER_TYPE = "user_type"
    static let KEY_ICON_IMAGE = "icon_image"
    static let KEY_STATUS_DISPLAY = "status_display"
    
    static let KEY_RATING = "rating"
    static let KEY_LANG_PREF = "lang_pref"
    static let KEY_LANGUAGE_PREFERENCES = "language_preferences"
    static let KEY_LANGUAGE_PREFERENCES_VALUES = "language_preferences_values"
    static let KEY_ENGLISH_NAME = "english_name"
    static let KEY_READ_ONLY = "readonly"
    
    static let KEY_FOLLOWUPDATA = "follow_up_data"
    static let KEY_FOLLOWUPID = "follow_up_id"
    static let KEY_FOLLOWUPREASON = "follow_up_reason"
    static let KEY_ISFOLLOWUP = "is_follow_up"
    
    static let KEY_MEDPLUS = "medplus"
    static let KEY_TIME_STAMP_C = "timeStamp"
    static let KEY_CURRENT_STATUS = "currentStatus"
    static let KEY_PRESCRIPTION_ORDER_ID = "prescription_order_id"
    static let KEY_PRODUCT_FORM = "productForm"
    static let KEY_MEDPLUS_ID = "medplus_id"
    static let KEY_ORDER_ID_U = "order_id"
    static let KEY_HAS_PRICE = "has_price"
    static let KEY_ORDER_AMOUNT_U = "order_amount"
    static let KEY_DELIVERED_ON = "delivered_on"
    
    static let KEY_CUSTOMERID = "customerId"
    static let KEY_ORDER_AMOUNT = "orderAmount"
    static let KEY_ORDER_ID = "orderId"
    static let KEY_AVAILABLE_QUANTITY = "availableQty"
    static let KEY_DISCOUNT_PERCENTAGE = "discountPercentage"
    static let KEY_MANUFACTERER = "manf"
    static let KEY_MRP = "mrp"
    static let KEY_PACK_SIZE = "packsize"
    static let KEY_PRICE = "price"
    static let KEY_PRODUCT_ID = "product_id"
    static let KEY_QTY = "qty"
    
    static let KEY_PINCODE = "pincode"
    static let KEY_FILE_NAME = "file_name"
    static let KEY_ENDS_AT = "ends_at"
    static let KEY_DETAILS = "details"
    static let KEY_SUBSCRIPTION = "subscription"
    static let KEY_SUBSCRIBED = "subscribed"
    static let KEY_ENABLE_COUPON = "enable_coupon"
    static let KEY_COUPON_CODE = "coupon_code"
    static let KEY_S_TYPE = "s_type"
    static let KEY_SENDER_TYPE = "sender_type"
    static let KEY_THREAD_ID = "thread_id"
    static let KEY_MESSAGES = "messages"
    static let KEY_LAST_MESSAGE = "latestMessage"
    static let KEY_USER = "user"
    static let KEY_BODY = "body"
    static let KEY_MRN_NO = "mrn_no"
    static let KEY_CALL_CHANNEL = "call_channel"
    static let KEY_OTP_SENT = "otp_sent"
    static let KEY_MOBILE_VERIFIED = "mobile_verified"
    static let KEY_OTP_CODE = "otp_code"
    static let KEY_MASKED_EMAIL = "masked_email"
    static let KEY_MASKED_MOBILE_NO = "masked_mobile_no"
    static let KEY_MRN = "mrn"
    
    static let KEY_JOB_ID  = "jobId"
    static let KEY_BOOKING_TYPE = "booking_type"
    static let KEY_NETWORK = "network"
    static let KEY_PUBLIC_PROFILE = "public_profile"
    static let KEY_TO_DATE = "to_date"
    static let KEY_NO_OF_DAYS = "medicine_intake_days"
    static let KEY_MEDICATION_STATE = "medication_state"
    static let KEY_FROM_DATE = "from_date"
    static let KEY_INTAKE_TIME = "intake_time"
    static let KEY_PASSWORD_CURRENT = "password_current"
    static let KEY_READ_AT = "read_at"
    static let KEY_NOTIFICATION_TYPE = "notifiable_type"
    static let KEY_NOTIFICATION_ID = "notifiable_id"
    static let KEY_TYPE = "type"
    static let KEY_PLAN_TYPE = "plan_type"
    static let KEY_TOKEN_ID = "tokenId"
    static let KEY_SESSION_ID = "sessionId"
    static let KEY_FCM_TOKEN = "fcm_token"
    static let KEY_APVOIP_TOKEN = "apvoip_token"
    static let KEY_DEVICE_TOKEN = "device_token"
    static let KEY_EXTRA_DATA = "extra_data"
    static let KEY_CITY = "city"
    static let KEY_PIN_CODE = "pin_code"
    static let KEY_MOBILE_NO = "mobile_no"
    static let KEY_STATE = "state"
    static let KEY_ALTERNATE_CONTACT_NO = "alternate_contact_no"
    static let KEY_COUNTRY_ID = "country_id"
    static let KEY_MEMBER_ID = "member_id"
    static let KEY_SERVICE_START_ID = "service_starts_at"
    static let KEY_SERVICE_END_ID = "service_ends_at"
    static let KEY_ADDRESS_1 = "address1"
    static let KEY_ADDRESS_2 = "address2"
    static let KEY_PREFIX = "prefix"
    static let KEY_NAME = "name"
    static let KEY_WEIGHT = "weight"
    static let KEY_HEIGHT = "height"
    static let KEY_DOES_SMOKE = "does_smoke"
    static let KEY_MEDICAL_HISTORY = "medical_history"
    static let KEY_LIFE_STYLE_ID = "life_style_activity_id"
    static let KEY_SLEEP_DURATION_ID = "sleep_duration_id"
    static let KEY_SLEEP_PATTERN_ID = "sleep_pattern_id"
    static let KEY_MARITAL_STATUS_ID = "marital_status_id"
    static let KEY_EXERCISE_PER_WEEK_ID = "exercise_per_week_id"
    static let KEY_SWITCH_MEDICINE_ID = "switch_in_medicine_id"
    static let KEY_MEDICAL_HISTORY_NEW_ID = "medical_history_id"
    static let KEY_LIFE_STYLE = "life_style_activity"
    static let KEY_SLEEP_DURATION = "sleep_duration"
    static let KEY_SLEEP_PATTERN = "sleep_pattern"
    static let KEY_MARITAL_STATUS = "marital_status"
    static let KEY_EXERCISE_PER_WEEK = "exercise_per_week"
    static let KEY_SWITCH_MEDICINE = "switch_in_medicine"
    static let KEY_SWITCH_MEDICINE_OTHERS = "switch_in_medicine_reason"
    static let KEY_MEDICAL_INSURANCE = "medical_insurance"
    static let KEY_ALLERGIES = "allergies"
    static let KEY_DRUG_ALLERGIES = "drug_allergies"
    static let KEY_MEDICATIONS  = "medications"
    static let KEY_MASTER_DATA  = "master_data"
    static let KEY_PREGNANCY_DETAILS  = "pregnancy_details"
    static let KEY_EXPECTING_MOTHER  = "expecting_mother"
    static let KEY_CONCEPTIONS  = "no_of_conceptions"
    static let KEY_COMPLICATIONS  = "complications_if_any"
    static let KEY_ABORTIONS  = "no_of_abortions"
    static let KEY_PATIENT = "patient"
    static let KEY_DOCTOR = "doctor"
    static let KEY_RATINGS = "ratings"
    static let KEY_APPOINTMENT_ID = "appointment_id"
    static let KEY_PUBLIC_APPOINTMENT_ID = "public_appointment_id"
    static let KEY_ATTACHMENTS = "attachments"
    static let KEY_ATTACHMENTS_NEW = "attachments_new"
    static let KEY_FINISHED_AT = "finished_at"
    static let KEY_STARTED_AT = "started_at"
    static let KEY_PRACTITIONER_NUMBER = "practitioner_number"
    static let KEY_AVATAR_URL = "avatar_url"
    static let KEY_SEPCIALIZATION = "specialization"
    static let KEY_LAST_NAME = "last_name"
    static let KEY_MIDDLE_NAME = "middle_name"
    static let KEY_TO = "to"
    static let KEY_TOTAL = "total"
    static let KEY_NEXT_PAGE_URL = "next_page_url"
    static let KEY_FROM = "from"
    static let KEY_LAST_PAGE = "last_page"
    static let KEY_CURRENT_PAGE = "current_page"
    static let KEY_USER_ID = "user_id"
    static let KEY_FAMILY_ID = "family_id"
    static let KEY_UPDATED_AT = "updated_at"
    static let KEY_CREATED_AT = "created_at"
    static let KEY_BOOKED_FOR = "booked_for"
    static let KEY_BOOKED_FOR_USER_ID = "booked_for_user_id"
    static let KEY_BOOKABLE = "bookable"
    static let KEY_AGE = "age"
    static let KEY_CALL_TYPE = "call_type"
    static let KEY_NOTES = "notes"
    static let KEY_DOCTOR_NOTES = "doctor_notes"
    static let KEY_SCHEDULED_AT = "scheduled_at"
    static let KEY_ACCESS_TOKEN = "access_token"
    static let KEY_REFRESH_TOKEN = "refresh_token"
    static let KEY_TOKEN_TYPE = "token_type"
    static let KEY_EXPIRES_IN = "expires_in"
    static let KEY_ERROR = "error"
    static let KEY_ERRORS = "errors"
    static let KEY_FIRST_NAME = "first_name"
    static let KEY_GRANT_TYPE = "grant_type"
    static let KEY_USERNAME = "username"
    static let KEY_BASE_URL = "base_url"
    static let KEY_CLIENT_SECRET = "client_secret"
    static let KEY_CLIENT_ID = "client_id"
    static let KEY_RINGING_DURATION = "ringing_duaration"
    static let KEY_FILE_ATTACHMENT_LIMIT = "file_attachment_limit"
    static let KEY_APPOINTMENT_CALL_BACK_TIME_LIMIT = "appointment_call_back_time_limit"
    static let KEY_CODE = "code"
    static let KEY_STATUS = "status"
    static let KEY_DATA = "data"
    static let KEY_HOTLINE = "hotline"
    static let KEY_IS_MINOR = "is_minor"
    static let KEY_NOTIFY = "notify"
    static let KEY_EMAIL = "email"
    static let KEY_RESET_OPTION = "reset-option"
    static let KEY_NEW_USER = "new_user"
    static let KEY_EMAIL_ID = "email_id"
    static let KEY_ID = "id"
    static let KEY_FULLNAME = "full_name"
    static let KEY_MCI_CODE = "mci_code"
    static let KEY_PASSWORD = "password"
    static let KEY_CONFIRM_PASSWORD = "password_confirmation"
    static let KEY_MEDIA_SOURCE = "media_source"
    static let KEY_UTM_CAMPAIGN = "utm_campaign"
    static let KEY_UTM_MEDIUM = "utm_medium"
    static let KEY_DOB = "dob"
    static let KEY_DATE_OF_BIRTH = "date_of_birth"
    static let KEY_GENDER = "gender"
    static let KEY_MESSAGE = "message"
    static let KEY_PIVOT = "pivot"
    static let KEY_DOCTOR_ID = "doctor_id"
    static let KEY_TIME_STAMP = "timestamp"
    static let KEY_CDN_PHOTO_URL = "cdn_photo_url"
    static let KEY_SUBSCRIPTION_ERROR = "subscription_error"
    static let KEY_COUPON_APPLIED = "coupon_applied"
    static let KEY_CURRENTPAGE = "currentPage"
    static let KEY_NEXTPAGE = "nextPage"
    static let KEY_PERPAGE = "perPage"
    static let KEY_PREVPAGE = "prevPage"
    static let KEY_FIRST_LEVEL_VALUE = "first_level_value"
    static let KEY_SYMPTOMS = "symptoms"
    static let KEY_SYMPTOM = "symptom"
    static let KEY_PROVISIONAL_DIAGNOSIS = "provisional_diagnosis"
    static let KEY_FOLOWUP_DATE = "follow_up_date"
    static let KEY_SYMPTOM_SEVERITY = "severity"
    static let KEY_MEDIPHARMA_TIME = "scheduled_at"
    static let KEY_WHITELABEL_CONFIG = "white_label_config"
    static let KEY_WHITELABEL_COLOR1 = "color_1"
    static let KEY_WHITELABEL_COLOR2 = "color_2"
    static let KEY_WHITELABEL_ENABLED = "white_label_enabled"
    static let KEY_WHITELABEL_EMPLOYER = "employer"
    static let KEY_WHITELABEL_TITLELOGO = "logo_png"
    
    static let KEY_ADDRESS_FULL = "adrs"
    static let KEY_PHONE_NUMBER = "phone_number"
    static let KEY_MOBILE_NUMBER = "mobile_number"
    static let KEY_ORDER_BY = "order_by"
    static let KEY_APPOINTMENT_DATE = "apt_dt"
    static let KEY_PRINT_OUT = "print_out"
    static let KEY_BENEFICIARY_ID = "beneficiary_id"
    static let KEY_BENEFICIARY_COUNT = "beneficiary_count"
    static let KEY_PACKAGE_ID = "package_id"
    static let KEY_REPORTS = "reports"
    static let KEY_TRACKING = "tracking_statuses"
    static let KEY_TSP = "tsp_details"
    static let KEY_APPOINTMENT_DETAILS = "appointment_details"
    
    static let KEY_ADDRESS_TITLE = "title"
    static let KEY_ADDRESS = "address"
    static let KEY_ADDRESS_LANDMARK = "landmark"
    static let KEY_ADDRESS_ID = "address_id"
    static let KEY_ADDRESS_DEFAULT = "is_default"
    static let KEY_PARTNER_ID = "partner_id"
    static let KEY_CART = "cart"
    static let KEY_APPOINTMENT_DIAG_ID = "apt_id"
    static let KEY_TRANSACTION_ID = "trans_id"
    static let KEY_PACKAGE_NAME = "package_name"
    static let KEY_PACKAGE_PRICE = "price"
    
    //Paytm keys
    
    static let KEY_PAYTM_CALLBACK_URL = "CALLBACK_URL"
    static let KEY_PAYTM_CHANNEL_ID = "CHANNEL_ID"
    static let KEY_PAYTM_CHECKSUMHASH = "CHECKSUMHASH"
    static let KEY_PAYTM_CUST_ID = "CUST_ID"
    static let KEY_PAYTM_INDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID"
    static let KEY_PAYTM_MID = "MID"
    static let KEY_PAYTM_ORDER_ID = "ORDER_ID"
    static let KEY_PAYTM_TXN_AMOUNT = "TXN_AMOUNT"
    static let KEY_PAYTM_WEBSITE = "WEBSITE"
    static let KEY_PAYTM_EMAIL = "EMAIL"
    static let KEY_PAYTM_MOBILE_NO = "MOBILE_NO"
    
    //Wellness Program Keys
    
    static let KEY_WELLNESS_BOOKING_ID = "id"
    static let KEY_WELLNESS_BOOKING_TIME = "expectedTime"
    static let KEY_WELLNESS_BOOKING_USERNAME = "customerName"
    static let KEY_WELLNESS_BOOKING_EMAIL = "customerEmailId"
    
    //Wellness Program Keys BOOKING WORKOUT- local server
    static let KEYS_WELLNESS_WORKOUT_ID = "workout_id"
    static let KEYS_WELLNESS_EXPECTED_TIME = "expected_time"
    static let KEYS_WELLNESS_BOOK_DATE = "booked_for_date"
    static let KEYS_WELLNESS_DISTANCE = "distance"

    //Wellness Workout List- loval server
    static let KEYS_WORKOUT_DATE = "workout_date"
    static let KEYS_WORKOUT_LAT = "point_lat"
    static let KEYS_WORKOUT_LON = "point_long"
    static let KEYS_WORKOUT_LOC = "location"
    
    //Wellness Workout List
    static let KEYS_WORKOUT_STUDIO_LOCATION = "location"
    static let KEYS_WORKOUT_STUDIO_LAT = "point_lat"
    static let KEYS_WORKOUT_STUDIO_LONG = "point_long"
    
    //Wellness Workout _ Cancel
    static let KEYS_WORKOUT_CANCEL_WORKOUTID = "workout_id"
    static let KEYS_WORKOUT_CANCEL_BOOKINGID = "booking_id"
    static let KEYS_WORKOUT_CANCEL_REASON = "cancel_reason"
    
    //Wellness Workout Attendance
    static let KEYS_WORKOUT_ATTENDANCE_BOOKINGID = "booking_id"
    static let KEYS_WORKOUT_ATTENDANCE_OUTLETID = "outlet_id"
    
    
    //HRA
    static let KEY_WAIST_CIRCUMFERENCE = "waist_circumference"
    static let KEY_KNOW_BP_READINGS = "know_bp_readings"
    static let KEY_SBP = "sbp"
    static let KEY_DBP = "dbp"
    static let KEY_DIABETES = "diabetes"
    static let KEY_BP = "blood_pressure"
    static let KEY_HYPER = "hypertension"
    static let KEY_CARDIO = "cardiovascular"
    static let KEY_ATRIAL = "atrial_fibrillation"
    static let KEY_VH = "ventricular_hypertrophy"
    static let KEY_PARENTAL_DIABETES = "parents_diabetic"
    static let KEY_PARENTAL_HBP = "parents_hbp"
    static let KEY_PARENTAL_CARDIO = "parents_cardiac_condition"
    static let KEY_SMOKE_CONDITION = "smoking_condition"
    static let KEY_CIGARETES = "cigarettes_per_day"
    static let KEY_PHYSICAL_ACTIVITY = "physical_activity_state"
    static let KEY_CALORIES_INTAKE = "calories_intake"
    static let KEY_BMI = "bmi"
    static let KEY_IBW = "ibw"
    static let KEY_BMR = "bmr"
    static let KEY_CALORIES_REQUIRED = "calories_required"
    static let KEY_DIABETES_RISK = "diabetes_risk"
    static let KEY_HYPER_RISK = "hypertention_risk"
    static let KEY_CVD_RISK = "cvd_risk"
    static let KEY_STROKE_RISK = "stroke_risk"
    
}

struct HTTPMethods {
    static let OPTIONS = "OPTIONS"
    static let GET = "GET"
    static let HEAD = "HEAD"
    static let POST = "POST"
    static let PUT = "PUT"
    static let PATCH = "PATCH"
    static let DELETE = "DELETE"
    static let TRACE = "TRACE"
    static let CONNECT = "CONNECT"
}

struct NETWORK_REQUEST_KEYS {
    static let KEY_CHARSET = "application/json;charset=UTF-8"
    static let KEY_APPLICATION_FORM_URLENCODED = "application/x-www-form-urlencoded"
    static let KEY_APPLICATION_JSON = "application/json"
    static let KEY_CONTENT_TYPE = "Content-Type"
    static let KEY_COOKIE = "Cookie"
    static let KEY_ACCEPT = "Accept"
    static let KEY_AUTHORIZATION = "Authorization"
    static let KEY_SESSION_ID = "sessionId"
    static let KEY_MULTIPART_FORM_DATA = "multipart/form-data"
    static let KEY_DOCONLINE_API = "DocOnline-Api"
    static let KEY_VALUE_DOCONLINE = "2018-12-10"
    static let KEY_ONE_TIME_VERSION_VALUE = "2018-08-13"
    static let REQUEST_TIME_OUT : TimeInterval = 90
}

struct MessageServer {
    ///used for staging server
    static let STAGING = "staging"
    ///used for production live
    static let PRODUCTION = "production"
    ///used for local
    static let LOCAl = "local"
    ///used for beta
    static let BETA = "beta"
    ///used for demo
    static let DEMO = "demo"

}


struct RazorpayCredentials {
    static let localRKey = "rzp_test_T1JNSjrdIIPHIh"//"rzp_test_1s329VFbWAzqEk"
    static let stagingRKey = "rzp_test_pGc2kHLLywFzbe"
    static let liveRKey = "rzp_live_x7EYqVpFpaC5RY"
}

public class App{
    
    
    ///VIOP device token
    static var apvoip_token = ""
    ///incomingcall duration details
    static var timeDuration = ""
    static var isIncomingCallRecieved = false
    static var call_appointment_id = ""

    
    static func getUserAccessToken() -> String{
        let defaults = UserDefaults.standard
        var headers = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU4OGQzMGZmYTBlMGE3OWI3Mzc3YmUyMTkxZGE5NmQ4ZGU3ZTI5MmY5NTI4OWE0OGMyZjRhZDIxMWVhY2ZhMjIzOWY4MWY1OTExMjljNTI5In0.eyJhdWQiOiIzIiwianRpIjoiNTg4ZDMwZmZhMGUwYTc5YjczNzdiZTIxOTFkYTk2ZDhkZTdlMjkyZjk1Mjg5YTQ4YzJmNGFkMjExZWFjZmEyMjM5ZjgxZjU5MTEyOWM1MjkiLCJpYXQiOjE1OTQyMDAxMTAsIm5iZiI6MTU5NDIwMDExMCwiZXhwIjoxNjI1NzM2MTEwLCJzdWIiOiIzNzE1Iiwic2NvcGVzIjpbIioiXX0.fbEIbyBIQD5ZpW3uxthkKxeV15ANC9GR1Vh00HTYR-Euu1mFSEWLDHohfUUALwV2mbAallfjvjFub9KHQoLmnc_cDDk4y-sGxLIgh0ZvkGTAPfciDwHZVEHESfla7GHMDelry9MHQEMa77SujX6CNBvn4Y-Sr7oRSef2qjEVDUBY2Uu8GUZ2BhEssvpvIsc8giSH-bzPXPoUxzAojLaZANe91lekRo9lnY7a7OX99oCb7c8LoLeV1MRzkhcRGjxYwnesBsioxl4jS-Ym9h9A7e4HRIqjjxoJ24gyAq8q-whDBgY--RE1y54PLnkstYwzK39wDWk7KgFjk6e6OeHP53dFflv4JK-7SuDU1y9fvqEHbmyNN1Md2UwDksZkN8SbEXglwxaIvDgqsoPyKNshA4hLcHS-gc2OTkwRsGYeLodzVy9vBk1lCiO0jgi55nQRr0_cAKyfnudoWDnhlfS_zRcoLz_nblVzcQ7dD3tObXmaa2aj4-EVi4VKkrEZcIj5NZxFvwiCDURlnxOLa6jJi2VdGAKJp0FF5UQLtIwxbtUG70WYxK15EtBZWGmTqqXNI4oYYaHAlPxC5OsC7I6LCSg9nvhQCAL-NUDLgQXtTvpM2fk3y2vMBphrkO_3RsdPkCwKefrXsldc3L2iC_zTTZakLP5x0zyYOJ3g8KC2hyw"
        if defaults.object(forKey: UserDefaltsKeys.ACCESS_TOKEN) != nil && defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) != nil{
            let access_token = defaults.object(forKey:  UserDefaltsKeys.ACCESS_TOKEN) as! String
            let token_type = defaults.object(forKey: UserDefaltsKeys.TOKENTYPE) as! String
            headers = "\(token_type) \(access_token)"
            print("Headers:=\(headers)")
        }else {
            print("No stored access token")
        }
        return headers
    }
    
    
    
    static func getDeviceInfo() -> String {
        var parameters = [String:String]()

        let currentDevice = UIDevice.current
        parameters["DeviceUDID"] = currentDevice.identifierForVendor?.uuidString
        parameters["DeviceName"] = currentDevice.name
        parameters["DeviceSystemName"] = currentDevice.systemName
        parameters["DeviceOSVersion"] = currentDevice.systemVersion
        parameters["DeviceModel"] = currentDevice.model
        
        var stringJson = ""
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            //converting jsondata string
            stringJson = String(data: jsonData, encoding: String.Encoding.utf8)!
            print("Decoded extra data string:\(stringJson)")

        } catch {
            print(error.localizedDescription)
        }
        
        return stringJson
    }
}



struct CallingStatus {
    ///on call recieved
    public static var CALL_STATUS_PATIENT_ACKNOWLEDGED = 7
    ///on call accepted
    public static var  CALL_STATUS_PATIENT_ACCEPTED = 8
    ///on call rejected
    public static var  CALL_STATUS_PATIENT_REJECTED = 9
}



struct UserDefaltsKeys {
    static let APP_HOTLINE_DTAIILS = "AppHotlineDetails"
    static let APP_SETTINGS_DTAIILS = "AppSettingsDetails"
    static let FAMILY_MEMBERS_COUNT = "FamilyMemCount"
    static let FAMILY_ALLOWED_COUNT = "FamilyAllowedCount"
    static let FAMILY_EXCEED_MESSAGE = "FamilyExceedMsg"
    
    static let DATE_OF_LOGIN = "LoggedInDateNTime"
    static let PREVIOUS_APVOIP_TOKEN = "PreviousAPVOIPToken"
    static let CURRENT_USER_ID = "CurrentUserID"
    static let LANGUAGE_PREFERENCES = "LanguagePreferences"
    static let LANGUAGE_PREFERENCES_VALUES = "LanguagePreferencesValues"
    static let IS_RATING_GIVEN_TO_DOCTOR = "IsRatingGivenToDoctor"
    static let APP_LOADED_FIRST_TIME = "IsAppLoadedFirstTime"
    static let MOBILE_NUMBER = "MobileNumber"
    static let IS_MOBILE_VERIFIED = "IsMobileVerified"
    static let IS_EMAIL_VERIFIED = "IsEmailVerified"
    static let IS_SOCIAL_LOGIN = "IsSocialLogin"
    static let KEY_SET_PASSWORD = "SetPassword"
    static let KEY_NETWORK_TYPE = "NetworkType"
    static let KEY_MEDIA_SOURCE = "media_source"
    static let KEY_UTM_CAMPAIGN = "utm_campaign"
    static let KEY_UTM_MEDIUM = "utm_medium"
    static let KEY_CALL_DURATION = "CallDurationText"
    //user profile
    static let KEY_LOGGED_USER_EMAIL = "LoggedUserEmail"
    static let KEY_FIRSTNAME = "UserFirstName"
    static let KEY_MRN_NO = "MRNNumber"
    static let KEY_FULL_NAME = "UserFullName"
    static let KEY_USER_PREFIX = "UserNamePrefix"
    static let KEY_USER_PREFIX_VALUE = "UserNamePrefixValue"
    static let KEY_USER_PROFILE_NAME = "UserProfileName"
    static let KEY_USER_MIDDLE_NAME = "UserMiddleName"
    static let KEY_USER_LAST_NAME = "UserLastName"
    static let KEY_USER_DATE_BIRTH = "UserDOB"
    static let KEY_USER_GENDER = "UserGender"
    static let KEY_USER_EMAIL = "UserEmail"
    static let KEY_USER_PHONE = "UserPhone"
    static let KEY_USER_ALTERNATE_PHONE = "UserAlternatePhone"
    static let KEY_USER_ADDRESS1 = "UserAddress1"
    static let KEY_USER_ADDRESS2 = "UserAddress2"
    static let KEY_USER_CITY = "UserCity"
    static let KEY_USER_STATE = "UserState"
    static let KEY_USER_PINCODE = "UserPincode"
    static let KEY_USER_COUNTRY = "UserCountry"
    static let KEY_USER_COUNTRY_CODE = "UserCountryCode"
    static let KEY_USER_STATUS = "UserProfileStored"
    
    static let KEY_REMOTE_STORED = "RemoteStored"
    static let KEY_NAME_PREFIXES = "NamePrefixes"
    static let KEY_NOTIFI_READ_COUNT = "NotifiReadCount"
    static let KEY_OPENTOK_KEY = "OpentTokKey"
    static let KEY_FB_CLIENT_ID = "fb_client_id"
    static let KEY_FB_SECRET = "fb_client_secret"
    static let KEY_HAS_LOGIN_KEY = "hasLoginKey"
    static let KEY_USER_NAME = "UserName"
    static let KEY_COUNTRIES_LIST = "CountriesList"
    static let KEY_ALLERGIES = "UserAllergies"
    static let KEY_DRUG_ALLERGIES  = "UserDrugAllergies"
    static let KEY_MEDICATIONS = "UserMedications"
    static let KEY_HEALTH_STATUS = "HealthUpdated"
    static let KEY_WEIGHT = "UserWeight"
    static let KEY_HEIGHT = "UserHeight"
    static let KEY_DOES_SMOKE = "UserDoesSmoke"
    static let KEY_LIFE_STYLE = "UserLifeStyle"
    static let KEY_SLEEP_DURATION = "UserSleepDuration"
    static let KEY_SLEEP_PATTERN = "UserSleepPattern"
    static let KEY_EXERCISE_PER_WEEK = "UserExercisePerWeek"
    static let KEY_MARITAL_STATUS = "UserMaritalStatus"
    static let KEY_SWITCH_MEDICINE = "UserSwitchInMedicine"
    static let KEY_SWITCH_MEDICINE_OTHERS = "UserSwitchInMedicineReason"
    static let KEY_MEDICAL_HISTORY_NEW = "UserMedicalHistoryNew"
    static let KEY_MEDICAL_HISTORY = "UserMedicalHistory"
    static let KEY_MEDICAL_INSURANCE = "UserMedicalInsurance"
    static let KEY_MASTER_DATA  = "UserMasterData"
    static let KEY_PREGNANCY_DATA  = "UserPregnancyData"
    static let KEY_TOKEN_REGISTRATION = "TokenRegistered"
    static let TOKENTYPE = "TokenType"
    static let ACCESS_TOKEN = "AccessToken"
    static let PREVIOUS_ACCESS_TOKEN = "PreviousAccessToken"
    static let PREVIOUS_TOKEN_TYPE = "PreviousTokenType"
    static let DID_USER_LOGED_OUT = "DidPreviousUserSessionExpired"
    static let KEY_BASE_URL = "BaseURL"
    static let KEY_CLIENT_SECRET = "ClientSecret"
    static let KEY_CLIENT_ID = "ClientID"
    static let KEY_RINGING_DURATION = "RingingDuration"
    static let KEY_FILE_ATTACHMENT_LIMIT = "FileAttachmentLimit"
    static let KEY_APPOINTMENT_CALL_BACK_TIME_LIMIT = "AppointCallBckLimit"
    static let KEY_REMOTE_CONFIG_FETCHED = "RemoteValuesFetched"
    
    static let KEY_DIAGNOSTICS_B2B_USER = "B2BUser"
    
    
}


struct NotificationActionIdentifiers {
    static let ViewAction = "ViewAction"
    static let DismissAction  = "DismissAction"
}



struct NotificationCategoryIdentifiers {
    static let AlertCategory = "AlertCategory"
    static let VoiceCall  = "VoiceCall"
    static let MissedCall = "MissedCall"
    static let Remainder = "Remainder"
}


struct AppURLS {
    
    struct BaseURLS {
        static let Staging    = "https://staging.doconline.com/"
        static let Beta       = "https://beta.doconline.com/"
        static let Demo       = "https://project.doconline.com/"
        static let Production = "https://app.doconline.com/"
    }
    
    static let URL_UserRegistration = BaseUrl + "api/user/register"
    static let URL_Appointments_create = BaseUrl + "api/user/appointments/create"
    static let URL_BookAppointment = BaseUrl + "api/user/appointments"
    static let URL_OAuth = BaseUrl + "oauth/token/"
    static let URL_UserDevices = BaseUrl + "api/user/devices/"
    static let URL_LeadSource = BaseUrl + "api/user/lead-source-update/"
    static let URL_HealthProfile = BaseUrl + "api/user/health-profile/"
    static let URL_Medications = BaseUrl + "api/user/health-profile/medication/"
    static let URL_Allergies = BaseUrl + "api/user/health-profile/allergy/"
    static let URL_Drug_Allergies = BaseUrl + "api/user/health-profile/drug-allergy/"
    static let URL_Countries = BaseUrl + "api/countries"
    static let URL_NamePrefixes = BaseUrl + "api/name-prefixes"
    static let URL_UserProfile = BaseUrl + "api/user/account/"
    static let URL_UserAvatar = BaseUrl + "api/user/account/avatar"
    static let URL_Notifications = BaseUrl + "api/user/notifications/"
    static let URL_ChangePassword = BaseUrl + "api/user/account/password"
    static let URL_ChanegMobileNumber = BaseUrl + "api/user/account/mobile-no"
    static let URL_SendOTP = BaseUrl + "api/user/account/mobile-no/send-otp"
    static let URL_VerifyOTP = BaseUrl + "api/user/account/mobile-no/verify"
    static let URL_Prescription = BaseUrl + "api/user/prescription/"
    static let URL_Family_mem = BaseUrl + "api/user/family"
    static let URL_Chat_connect = BaseUrl + "api/user/thread/"
    static let URL_Logout = BaseUrl + "api/user/logout"
    static let URL_Call_recieved = BaseUrl + "api/user/appointments/"
    static let URL_Forgot_password = BaseUrl + "api/user/password/email/"
    static let URL_Forgot_password_resetOptions = BaseUrl + "api/user/password/reset-options"
    static let URL_User_state = BaseUrl + "api/user/state/"
    static let URL_Procure_medicine = BaseUrl + "api/user/procure/prescription/"
    static let URL_Order_Medicines =  BaseUrl + "api/user/order/prescription/"
    static let URL_Orders_History = BaseUrl + "api/user/orders"
    static let URL_Languages = BaseUrl + "api/languages"
    static let URL_Set_lang_preferences = URL_UserProfile + "languages"
    static let URL_PASSWORD_CHECK = BaseUrl + "api/user/check-access/password/"
    static let URL_RAZORPAY_PLANS =  BaseUrl + "api/plans/"
    static let URL_ONE_TIME_PLAN = BaseUrl + "api/plans/onetime"  //newwly added
    static let URL_GET_OREDER_ID_ONE_TIME_PAYMENT = BaseUrl + "api/user/payment/subscription/orderid" //newly added
    static let APPLY_DISCOUNT_ONE_TIME_PAYMENT = BaseUrl + "api/user/discount-code"
    static let URU_APPLY_DISCOUNT_ONE_TIME_PAYMENT = BaseUrl + "api/unreg/discount-code"
    static let URL_ONE_TIME_PAYMENT_SUCCESS = BaseUrl + "api/user/payment/subscription/success" //newly added

    static let URL_SUBSCRIBE_NOW =  BaseUrl + "api/user/subscription/"
    static let URL_BILLINGS = BaseUrl + "api/user/billings"
    static let URL_CANCEL_SUBSCRIPTION = URL_SUBSCRIBE_NOW + "cancel/current"
    static let URL_CONSULTATION_CALL_CHECK = BaseUrl + "api/user/consultation/session/"
    static let URL_SUBSCRIPTION_UPGRADE = BaseUrl + "api/user/subscription/upgrade/"
    static let URL_PROMO_CODE = BaseUrl + "api/user/promo-code"
    static let URL_MEDICINES_READY_TO_ORDER_CHECK = BaseUrl + "api/user/procure/prescription/pending"
    
    static let URUSER_VALIDATE_BOOKING_DETAILS = BaseUrl + "api/validate/booking"
    static let URUSER_VALIDATE_USER_DETAILS = BaseUrl +  "api/validate/unregistered"
    static let URUSER_ATTACHEMTENTS =  BaseUrl + "api/unreg/attachments"
    static let URUSER_SESSION_FOR_COOKIES =  BaseUrl + "api/unreg/session"
    static let URUSER_BOOK_APPOINTMENT = BaseUrl + "api/unreg/book-an-appointment"
    static let URUSER_PAYMENT_SUCCESS = BaseUrl + "api/unreg/payment-success"
    static let URUSER_OT_PAYEMENT_SUCCESS = BaseUrl + "api/unreg/payment-success/onetime"
    static let URUSER_PAYTM_CHECKSUM = BaseUrl + "api/unreg/payment/subscription/generateChecksum"
    
    static let URL_PASSWORD_RESET = BaseUrl + "api/user/password/reset"
    
    static let SEND_EMAIL_VERIFICATION_CODE = BaseUrl + "api/user/account/email/send-otp/"
    static let VERIFY_MAIL = BaseUrl + "api/user/account/email/verify"
    static let MobileVerification = BaseUrl + "api/user/settings/mobile-no"
    static let EmailVerification = BaseUrl + "api/user/settings/email"
    
    static let CONSENT_STATUS_FAMILY_MEM = BaseUrl + "api/user/consent/status/"
    
    static let URL_Diagnostics_plans = BaseUrl + "api/user/diagnostics/packages/get-list"
    static let URL_Diagnostics_History = BaseUrl + "api/user/diagnostics/appointment/history"
    static let URL_DIAGNOSTICS_APPOINTMENT = BaseUrl + "api/user/diagnostics/appointment/create"
    static let URL_DIAGNOSTICS_APPOINTMENT_CONFIRM = BaseUrl + "api/user/diagnostics/appointment/booking-confirm"
    static let URL_DIAGNOSTICS_SEARCH = BaseUrl + "api/user/diagnostics/packages/search/"
    static let URL_DIAGNOSTICS_PINCODE = BaseUrl + "api/user/diagnostics/service-availability"
    static let URL_CART_COLLECTION = BaseUrl + "api/user/cart/get-collection"
    static let URL_CART_ADD = BaseUrl + "api/user/cart/add-to-cart"
    static let URL_CART_REMOVE = BaseUrl + "api/user/cart/remove-from-cart"
    static let URL_ADDRESS_ADD = BaseUrl + "api/user/address/add"
    static let URL_ADDRESS_LIST = BaseUrl + "api/user/address/get-list"
    static let URL_ADDRESS_DEFAULT = BaseUrl + "api/user/address/make-default"
    static let URL_ADDRESS_DELETE = BaseUrl + "api/user/address/delete"
    static let URL_ADDRESS_UPDATE = BaseUrl + "api/user/address/update"
    static let URL_DIAGNOSTICS_TIME_SLOTS = BaseUrl + "api/user/diagnostics/appointment/time-slots/"
    static let URL_DIAGNOSTICS_APPOINTMENT_SUMMARY = BaseUrl + "api/user/diagnostics/appointment/summery/"
    
    static let PRESCRIPTION_HAS_ORDER = BaseUrl + "api/user/procure/prescription/hasorder/"
    
    static let REQUEST_ACTIVATION_LINK_FAMILY_MEMBER = BaseUrl + "api/user/activation/notification/"
    
    static let URL_PAYTM_CHECKSUM = BaseUrl + "api/user/payment/subscription/generateChecksum"
    
    //vitals
    static let URL_VITALS_TEMPLATE = BaseUrl + "api/ehr/templates/vitals"
    static let URL_GET_VITAL_RECORDS = BaseUrl + "api/user/ehr/vitals"
    
    //users family limit
    static let URL_FAMILY_MEM_LIMIT = BaseUrl + "api/user/family-members/limit/"
    static let URL_APP_SETTINGS = BaseUrl + "api/user/settings"
    static let URL_PUBLIC_APP_SETTINGS = BaseUrl + "api/settings"
    
    //ehr
    static let URL_EHR_FILE_UPLOAD = BaseUrl + "api/user/ehr/file/"
    static let URL_PRESCRIPTIONS = BaseUrl + "api/user/ehr/prescription"
    static let URL_EHR_CONSENT = BaseUrl + "api/user/ehr/consent"
    
    //vitals image url
    static let VITAL_INFO_IMAGE_URL =  "https://d25gtz8j81w8po.cloudfront.net/production/assets/doconline-vitals-info.png"
    
    //Medication reminder
    static let URL_MEDICATION_SEARCH = BaseUrl + "api/user/medicine/search/?search="
    
    //HRA
    static let URL_SAVE_HRA = BaseUrl + "api/user/hra/save-results"
    static let URL_GET_HRA_VALIDATION = BaseUrl + "api/user/hra/input-limits"
    
    //Wellness Program
    static let URL_WELLNESS_CLASSES = "https://api.fitmein.in/api/Business/WorkoutsListByDate"
    static let URL_WELLNESS_CLASS_DETAILS = "https://api.fitmein.in/api/Business/WorkoutDetailsDEtails/"
    static let URL_WELLNESS_BOKKING = "https://api.fitmein.in/api/Business/ConformBooking"
    
    //Wellness Program-local server
    static let URL_WELLNESS_BOKKING_LOCAL = BaseUrl + "api/user/workout/register"
    static let URL_WELLNESS_WORKOUT_LIST = BaseUrl + "api/user/workout/list/"
    static let URL_WELLNESS_WORKOUT_DETAILS = BaseUrl + "api/user/workout/details/"
    static let URL_WELLNESS_WORKOUT_TRACKER = BaseUrl + "api/user/workout/history/"
    static let URL_WELLNESS_WORKOUT_CANCEL = BaseUrl + "api/user/workout/cancel"
    static let URL_WELLNESS_WORKOUT_IMAGE = BaseUrl + "api/user/workout/image/"
    static let URL_WELLNESS_WORKOUT_ATTENDANCE = BaseUrl + "api/user/workout/mark-attendance"
    static let URL_WELLNESS_STUDIO_LIST = BaseUrl + "api/user/workout/outlets/list"
    static let URL_WELLNESS_STUDIO_WORKOUT_LIST = BaseUrl + "api/user/workout/outlet/classes/"
    
    //BetterPlace
    static let URL_BETTER_PLACE = BaseUrl + "api/customer/user-login"
}



extension UIApplication {
    /**
     Method returns the top viewcontroller which is presenting now
     */
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
