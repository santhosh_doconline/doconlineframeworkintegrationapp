//
//  ViewController.swift
//  DoconlineChildApp
//
//  Created by Santosh Kumar on 06/07/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import UIKit
import Firebase
import DoconlineTestFramework

class Dashboard: UIViewController {
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mobNum: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var accessToken = ""
    
    var doconlineVC: DoconlineVC!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        //Observer to capture incoming voip token
        NotificationCenter.default.addObserver(self, selector:#selector(getIncomingPushCreds(_:)), name: NSNotification.Name ("voip_token"),object: nil)
        
        //Observer to capture incoming voip payload
        NotificationCenter.default.addObserver(self, selector:#selector(getIncomingPushPayload(_:)), name: NSNotification.Name ("voip_payload"),object: nil)
        
        //Intiating Doconline SDK Dashboard
        let storyboard = UIStoryboard(name: "MainSto", bundle: Bundle(for: DoconlineVC.self))
        doconlineVC = storyboard.instantiateViewController(withIdentifier: "DoconlineVC") as? DoconlineVC
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Func to handle above defined observer
    @objc func getIncomingPushCreds(_ notification: Notification)
    {
        doconlineVC.getIncomingPushCreds(notification)
    }
    
    //Func to handle above defined observer
    @objc func getIncomingPushPayload(_ notification: Notification)
    {
        doconlineVC.getIncomingPushPayload(notification)
    }
    
    //Action to handle onClick Doconline Dashboard
    @IBAction func showDashboard(_ sender: UIButton) {
        let param = "first_name=\(firstName.text!)&middle_name=&last_name=xyz&email=\(email.text!)&mobile_no=\(mobNum.text!)& date_of_birth=2000-01-26&gender=Female&country_id=356&member_id=DOC00745&service_starts_at=2020-01-01 12:11:26&service_ends_at=2022-12-31 12:11:26"
        let escapedString = param.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        performLogin("https://project.doconline.com/api/customer/user-login", param, targetAppUrl: "doconline:/betterplace/?"+escapedString!)
    }
    
    //Func that presents Doconline Dashboard screen
    func callDoconline(){
        doconlineVC.accessToken = self.accessToken
        doconlineVC.environment = .demo
        self.present(doconlineVC, animated: true, completion: nil)
    }
    
    //Func to handle register/login and capture required tokens
    func performLogin(_ actionURL:String,_ dataToPost:String, targetAppUrl: String) {
            resignFirstResponder()
        print("url is \(actionURL)")
            let mySession = URLSession.shared
            let url = URL(string: actionURL)
            var request = URLRequest(url:url!)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("doconlineApp", forHTTPHeaderField: "x-bypass-from")
            request.setValue("Bearer QFLqog5py2guJPluS4hvw86n26PHNp1r", forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            request.timeoutInterval = 90
            request.httpBody = dataToPost.data(using: String.Encoding.utf8)
            let task = mySession.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
//                print("status code \(response)")
                if let error = error
                {
                    print("Error==> : \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        return
                    }
                }
                if let response = response
                {
                    print("inside response")
                    let httpResponse = response as! HTTPURLResponse
                    print(response)
                    print(data)
                    do{
                        if let resultJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary {
                            print(resultJSON)
                                ///here checking if response has error key then perform action
                                    if let statusCode = resultJSON.object(forKey: "code") as? Int, statusCode != 200
                                    {
                                        if let errMsg = resultJSON.object(forKey: "message") as? String
                                        {
                                            DispatchQueue.main.async(execute: {
                                                self.activityIndicator.stopAnimating()
                                                let alert = UIAlertController(title: "Alert!!", message: errMsg, preferredStyle: .alert)
                                                let ok = UIAlertAction(title: "ok", style: .cancel, handler: nil)
                                                alert.addAction(ok)
                                                self.present(alert, animated: true, completion: nil)
                                            })
                                        }
                                    }
                                
                                else
                                {
                                    if httpResponse.statusCode == 200
                                    {
                                                DispatchQueue.main.async(execute: {
                                                    
                                                    if let info = resultJSON.object(forKey: "data") as? NSDictionary, let token_type = info.object(forKey: "token_type") as? String, let access_token = info.object(forKey: "access_token") as? String
                                                    {
                                                        self.activityIndicator.stopAnimating()
                                                        self.accessToken = token_type+" "+access_token
                                                        self.callDoconline()
                                                    }else {
                                                        self.activityIndicator.stopAnimating()
                                                    }
                                                })
                                    }
                                }
                            
                        }else{
                            print("error parsing json")
                        }
                    }catch let error {
                        print("error in json parsing")
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
            })
            task.resume()
        }
}

