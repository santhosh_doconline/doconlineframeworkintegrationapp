//
//  AppDelegate.swift
//  DoconlineChildApp
//
//  Created by Santosh Kumar on 06/07/20.
//  Copyright © 2020 Santosh Kumar. All rights reserved.
//

import UIKit
import PushKit
import CallKit
import Firebase
import Foundation
import AVFoundation
import DoconlineTestFramework

/////stores Opentok session id
//var callSessionID = ""
//
//var callDoctorModal : Doctor!
//
//
/////stores Opentok token id
//var callTokenid = ""
//
/////This variable stores integer to check call type **Audio** or **Video**
//var incomingCallType = 0
//
/////to check notification type
//var incomingNotificationType = ""
//
/////Boolean to check if already calling view is presenting or not
//var isCallingViewPresenting = false

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate
//, MessagingDelegate
{

    //MARK:- PushKit Implementation
    ///Registering For VOIP Push notifications
    let pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
  

    var gcmMessageIDKey = "gcm.message_id"
    

    ///Appdelegate Shared instance
       class var shared: AppDelegate {
           return UIApplication.shared.delegate as! AppDelegate
       }
    
    ///UIWindow instance variable
    var window: UIWindow?
    

//    //let appConfig = AppConfig()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        //        AppEnvironment.shared().environment = .debug("http://10.20.0.23:8000/", "2", "test")//.production
//                AppEnvironment.shared().environment = .debug("http://df2cbf178be8.ngrok.io/", "2", "test")//.ngrok
        //        AppEnvironment.shared().environment = .staging
//                AppEnvironment.shared().environment = .production
                AppEnvironment.shared().environment = .demo
        NSLog("testmaga")
//        if #available(iOS 10.0, *) {
//          // For iOS 10 display notification (sent via APNS)
//          UNUserNotificationCenter.current().delegate = self
//
//          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//          UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: {_, _ in })
//        } else {
//          let settings: UIUserNotificationSettings =
//          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//          application.registerUserNotificationSettings(settings)
//        }
//
//        application.registerForRemoteNotifications()
//
//        Messaging.messaging().delegate = self
        
//        FirebaseApp.configure()
//    //    appConfig.setupConfig()
        
//       // appConfig.currentUUID = UUID()
//      //  AppConfig.shared = appConfig
//        let token = Messaging.messaging().fcmToken
//        print("FCM token: \(token ?? "")")

        //MARK:- PushKit Implementation
          pushRegistry.delegate = self
          pushRegistry.desiredPushTypes = [.voIP]
          //end of Pushkit implementation
        
        return true
        
        
    }
    
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//      print("Firebase registration token: \(fcmToken)")
//        if let token = fcmToken{
//
//
//        let dataDict:[String: String] = ["token": token]
//      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//        }
//      // TODO: If necessary send token to application server.
//      // Note: This callback is fired at each app startup and whenever a new token is generated.
//    }
    
    

    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken as Data
//    }
    
    
    
    

//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//      // If you are receiving a notification message while your app is in the background,
//      // this callback will not be fired till the user taps on the notification launching the application.
//      // TODO: Handle data of notification
//
//      // With swizzling disabled you must let Messaging know about the message, for Analytics
//      // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//      // Print message ID.
//      if let messageID = userInfo[gcmMessageIDKey] {
//        print("Message ID: \(messageID)")
//      }
//        print("notification type \(userInfo["message"])")
//        if let notificationType = userInfo["notification_type"] as? String
//        {
//            print("1st level")
//        if notificationType.contains("ThreadClosedNotification")
//        {
//
//            print("** performing chat session exit")
//            if let thread_id = userInfo["threadId"] as? String {
//                if let closedby = userInfo["closed_by"] as? String {
//                    print("Notification thread id:\(thread_id) closed by:\(closedby)")
//
//                    appConfig.didReceiveEndChatNotification(threadID: thread_id, closedBy: closedby)
//                }
//            }
//        }
//        }
//
//      // Print full message.
//      print(userInfo)
//
//      completionHandler(UIBackgroundFetchResult.newData)
//    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
}



//MARK : - PushKit Implementation with voip
extension AppDelegate : PKPushRegistryDelegate {


    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        print("\(#function) voip token: \(pushCredentials.token)")
                
                let deviceToken = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1) })
                App.apvoip_token = deviceToken
        NotificationCenter.default.post(name: Notification.Name("voip_token"), object: nil, userInfo: ["pushInfo": ["pushCreds": pushCredentials, "apvoip_token": deviceToken]])
        NSLog("didupdate push registry appdelegate")
        ///call pushRegsitry func here
//        appConfig.didUpdatePushRegistry(pushCredentials: pushCredentials)
    }



    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        print("\(#function) incoming voip notfication: \(payload.dictionaryPayload)")
        NSLog("didreceive incoming call - appdelegate")
        NotificationCenter.default.post(name: Notification.Name("voip_payload"), object: nil, userInfo: ["incomingPush": ["incomingPayload": payload, "pushPayload": payload.dictionaryPayload]])
//        appConfig.didReceiveIncomingPush(didReceiveIncomingPushWith: payload, voipPushPayload: payload.dictionaryPayload)


    }
    
    

    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("\(#function) token invalidated")
    }






}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUNNotificationSoundName(_ input: String) -> UNNotificationSoundName {
    return UNNotificationSoundName(rawValue: input)
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIBackgroundTaskIdentifier(_ input: UIBackgroundTaskIdentifier) -> Int {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}
//
//@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//
//  // Receive displayed notifications for iOS 10 devices.
//  func userNotificationCenter(_ center: UNUserNotificationCenter,
//                              willPresent notification: UNNotification,
//    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//    let userInfo = notification.request.content.userInfo
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//    // Print message ID.
//    if let messageID = userInfo[gcmMessageIDKey] {
//      print("Message ID: \(messageID)")
//    }
//
//    // Print full message.
//    print(userInfo)
//
//    // Change this to your preferred presentation option
//    completionHandler([[.alert, .sound]])
//  }
//
//  func userNotificationCenter(_ center: UNUserNotificationCenter,
//                              didReceive response: UNNotificationResponse,
//                              withCompletionHandler completionHandler: @escaping () -> Void) {
//    let userInfo = response.notification.request.content.userInfo
//    // Print message ID.
//    if let messageID = userInfo[gcmMessageIDKey] {
//      print("Message ID: \(messageID)")
//    }
//
//    // Print full message.
//    print(userInfo)
//
//    completionHandler()
//  }
//}
